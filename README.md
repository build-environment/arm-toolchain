# arm-toolchain

[![license](https://img.shields.io/gitlab/license/build-environment/arm-toolchain)](https://gitlab.com/build-environment/arm-toolchain)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

`arm-toolchain` is a docker image based on
[build-environment](https://gitlab.com/build-environment/build-environment)
which provides provides all tools to build and debug application
using `arm-none-eabi` toolchain.

This `arm-toolchain` docker image provides all tools to build and
debug application using `arm-none-eabi` toolchain.

## How-to build the docker image

To build the image, use the Makefile:

```shell
$ make build
...
```

## How-to launch the docker image

The `arm-toolchain` docker image can be started using `arm-toolchain` script:

```shell
$ ./arm-toolchain [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
```
