#!/bin/bash

set -e -o pipefail

arm-none-eabi-gcc --version | grep arm-none-eabi-gcc | sed 's/(.\+) \([0-9\.]\+\) .*/\1/'
cmake --version | grep version | sed 's/version //'
make --version | grep Make | sed 's/GNU //'
python --version
gcovr --version | grep ^gcovr
lcov --version | sed 's/: LCOV version//'
gdb --version | grep gdb | sed 's/GNU \(gdb\) (.*)/\1/'
valgrind --version | sed 's/-/ /'
git --version | sed 's/version //'
git-lfs --version | sed 's/\/\([0-9\.]\+\) (.\+)/ \1/'
repo --version | grep launcher | sed 's/version //'
