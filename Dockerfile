ARG FROM=registry.gitlab.com/build-environment/build-environment:latest

# ------------------------------------------------------------------------------
# Pull base image
FROM ${FROM}

# ------------------------------------------------------------------------------
# set locale
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# ------------------------------------------------------------------------------
# Install tools via apt
RUN export DEBIAN_FRONTEND=noninteractive \
    && set -x \
    && apt-get -y update \
    # Install add-apt-repository
    && apt-get -y install \
        software-properties-common \
    # Setup git apt-repository
    # https://git-scm.com/download/linux
    && add-apt-repository ppa:git-core/ppa \
    # Setup git-lfs apt-repository
    # https://github.com/git-lfs/git-lfs/blob/main/INSTALLING.md
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash \
    && apt-get -y install \
        git \
        git-lfs \
        python3 \
        python3-dev \
        python3-setuptools \
        python3-pip \
        build-essential \
        doxygen \
        ccache \
        gdb \
        valgrind \
        lcov \
        clang-format \
    && apt-get clean && rm -rf /var/lib/apt/lists \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# ------------------------------------------------------------------------------
# Install git-repo
# https://github.com/GerritCodeReview/git-repo
RUN set -x \
    && curl https://raw.githubusercontent.com/GerritCodeReview/git-repo/stable/repo > /usr/bin/repo \
    && curl https://raw.githubusercontent.com/GerritCodeReview/git-repo/stable/completion.bash > /etc/bash_completion.d/git-repo \
    && chmod +rx /usr/bin/repo


# ------------------------------------------------------------------------------
# Install arm-none-eabi-gcc
# https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads
WORKDIR /opt
RUN ARCH=$(uname -m) \
    && GCC_VERSION="12.2.rel1" \
    && ARTIFACT_NAME="arm-gnu-toolchain-${GCC_VERSION}-${ARCH}-arm-none-eabi" \
    set -x \
    && wget -qO- https://developer.arm.com/-/media/Files/downloads/gnu/${GCC_VERSION}/binrel/${ARTIFACT_NAME}.tar.xz \
        | tar --transform "s/^${ARTIFACT_NAME}/arm-none-eabi-gcc/" -xJ
ENV PATH="${PATH}:/opt/arm-none-eabi-gcc/bin/"

# ------------------------------------------------------------------------------
# Install python packages
COPY resources/requirements.txt /tmp/requirements.txt
RUN set -x \
    && pip3 --no-cache-dir install --upgrade pip \
    && pip3 --no-cache-dir install --requirement /tmp/requirements.txt \
    && rm /tmp/requirements.txt

# ------------------------------------------------------------------------------
# Set working directory
ARG WORKDIR=/root

# ------------------------------------------------------------------------------
# Display, check and save environment settings
WORKDIR ${WORKDIR}
COPY resources/tools_version.sh /tmp/tools_version.sh
RUN set -e \
    && /tmp/tools_version.sh | tee env_settings \
    && rm /tmp/tools_version.sh

CMD [ "/bin/bash" ]
